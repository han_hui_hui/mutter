%global glib_version 2.71.0
%global gtk3_version 3.19.8
%global gsettings_desktop_schemas_version 40
%global json_glib_version 0.12.0
%global libinput_version 1.19.0
%global pipewire_version 0.3.33
%global lcms2_version 2.6
%global colord_version 1.4.5
%global mutter_api_version 12

Name:          mutter
Version:       44.6
Release:       2
Summary:       Window and compositing manager based on Clutter
License:       GPLv2+
URL:           https://www.gnome.org
Source0:       https://download.gnome.org/sources/%{name}/44/%{name}-%{version}.tar.xz

Patch0:        0001-window-actor-Special-case-shaped-Java-windows.patch
Patch1:        mutter-42.alpha-disable-tegra.patch

BuildRequires: meson pam-devel zenity sysprof-devel gtk-doc gettext-devel git-core
BuildRequires: xorg-x11-server-Xorg xorg-x11-server-Xvfb desktop-file-utils
BuildRequires: mesa-libEGL-devel mesa-libGLES-devel mesa-libGL-devel mesa-libgbm-devel
BuildRequires: pkgconfig(libpipewire-0.3) >= %{pipewire_version}
BuildRequires: pkgconfig(json-glib-1.0) >= %{json_glib_version}
BuildRequires: pkgconfig(libinput) >= %{libinput_version}
BuildRequires: pkgconfig(gsettings-desktop-schemas) >= %{gsettings_desktop_schemas_version}
BuildRequires: pkgconfig(lcms2) >= %{lcms2_version}
BuildRequires: pkgconfig(colord) >= %{colord_version}
BuildRequires: pkgconfig(gobject-introspection-1.0) >= 1.41.0
BuildRequires: pkgconfig(sm)
BuildRequires: pkgconfig(libwacom)
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xdamage)
BuildRequires: pkgconfig(xext)
BuildRequires: pkgconfig(xfixes)
BuildRequires: pkgconfig(xi)
BuildRequires: pkgconfig(xrandr)
BuildRequires: pkgconfig(xrender)
BuildRequires: pkgconfig(xcursor)
BuildRequires: pkgconfig(xcomposite)
BuildRequires: pkgconfig(x11-xcb)
BuildRequires: pkgconfig(xkbcommon)
BuildRequires: pkgconfig(xkbcommon-x11)
BuildRequires: pkgconfig(xkbfile)
BuildRequires: pkgconfig(xtst)
BuildRequires: pkgconfig(glesv2)
BuildRequires: pkgconfig(graphene-gobject-1.0)
BuildRequires: pkgconfig(sysprof-capture-4)
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(xkeyboard-config)
BuildRequires: pkgconfig(libcanberra)
BuildRequires: pkgconfig(gnome-settings-daemon)
BuildRequires: pkgconfig(gbm)
BuildRequires: pkgconfig(gnome-desktop-3.0)
BuildRequires: pkgconfig(gudev-1.0)
BuildRequires: pkgconfig(libdrm)
BuildRequires: pkgconfig(libstartup-notification-1.0)
BuildRequires: pkgconfig(wayland-eglstream)
BuildRequires: pkgconfig(wayland-protocols)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: xorg-x11-server
BuildRequires: pkgconfig(xwayland)
BuildRequires: chrpath

Requires: gnome-control-center-filesystem
Requires: gsettings-desktop-schemas%{?_isa} >= %{gsettings_desktop_schemas_version}
Requires: gnome-settings-daemon
Requires: gtk3%{?_isa} >= %{gtk3_version}
Requires: json-glib%{?_isa} >= %{json_glib_version}
Requires: libinput%{?_isa} >= %{libinput_version}
Requires: pipewire%{_isa} >= %{pipewire_version}
Requires: startup-notification
Requires: dbus
Requires: zenity
Recommends: mesa-dri-drivers%{?_isa}
Provides: firstboot(windowmanager) = mutter
Provides: bundled(cogl) = 1.22.0
Provides: bundled(clutter) = 1.26.0

%description
Mutter is a window and compositing manager based on Clutter, forked
from Metacity.

%package       devel
Summary:       Development files and Header files for %{name}
Requires:      %{name} = %{version}-%{release}
Requires:      mesa-libEGL-devel
Provides:      %{name}-tests
Obsoletes:     %{name}-tests < %{version}-%{release}
%description   devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1 

%build
%meson -Degl_device=true -Dwayland_eglstream=true 
#-Dxwayland_initfd=disabled
%meson_build

%install
%meson_install
chrpath -d %{buildroot}%{_bindir}/mutter
chrpath -d %{buildroot}%{_libdir}/mutter-%{mutter_api_version}/*.so.*
chrpath -d %{buildroot}%{_libdir}/lib*.so.*

%delete_la_and_a

%find_lang %{name}


mkdir -p %{buildroot}/etc/ld.so.conf.d
%ifarch sw_64
echo "/usr/lib/mutter-11" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf
%else
echo "/usr/lib64/mutter-11" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%license COPYING
%{_bindir}/mutter
%{_libdir}/lib*.so.*
%{_libdir}/mutter-%{mutter_api_version}/
%{_libexecdir}/mutter-restart-helper
%{_libexecdir}/mutter-x11-frames
%{_datadir}/GConf/gsettings/mutter-schemas.convert
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.wayland.gschema.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-*.xml
%{_udevrulesdir}/61-mutter.rules
%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%files devel
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_libexecdir}/installed-tests/mutter-%{mutter_api_version}
%{_datadir}/installed-tests/mutter-%{mutter_api_version}
%{_datadir}/mutter-%{mutter_api_version}/tests

%files help
%defattr(-,root,root)
%doc NEWS
%{_mandir}/man1/*.1.gz

%changelog
* Mon Feb 19 2024 hanhuihui <hanhuihui5@huawei.com> - 44.6-2
- rebuild for glib2 without sysprof

* Wed Nov 22 2023 lwg <liweiganga@uniontech.com> - 44.6-1
- update to version 44.6

* Mon Mar 13 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.2-2
- remove rpath

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.2-1
- Update to 43.2

* Wed Dec 14 2022 wuzx<wuzx1226@qq.com> - 42.2-3
- Add sw64 architecture

* Sun Jun 26 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-2
- Add BuildRequires pkgconfig(xwayland)

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-1
- Update to 42.2

* Fri Sep 17 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.4-3
- Add concrete dynamic library search path

* Wed Aug 25 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.38.4-2
- DESC: remove unnecessary BuildRequires

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.4-1
- Upgrade to 3.38.4
- Update Version, Release, BuildRequires, Obsoletes
- Delete patches which existed in new version, add one patch
- Use meson rebuild. update stage 'install', 'files'

* Wed Aug 5 2020 orange-snn <songnannan2@huawei.com> - 3.30.1-8
- change mesa-libEGL-devel to libglvnd-devel in buildrequires

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the files

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-6
- Package init
